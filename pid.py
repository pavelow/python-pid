class pid:
    def __init__(self, _Kp, _Ki, _Kd):
        self.Kp = _Kp
        self.Ki = _Ki
        self.Kd = _Kd
        self.P = 0
        self.I = 0
        self.D = 0
        self.lastError = 0

    def reset(self, error):
        self.lastError = error

    def process(self, error, dt):
        self.P = error
        self.I = self.I + error*dt
        self.D = (error - self.lastError)/dt
        self.lastError = error
        return (self.P * self.Kp) + (self.I * self.Ki) + (self.D * self.Kd)
