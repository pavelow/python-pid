#! /usr/bin/env python3

import pygame
from pygame.locals import *

from pid import pid

Width = 800
Height = 600
last_time = 0

if __name__ == "__main__":
    pygame.init()
    screen = pygame.display.set_mode((Width, Height))

    x_controller = pid(100,0,20)
    x_position = Width/2
    x_target = Width/2
    x_velocity = 0
    x_force = 0

    y_controller = pid(100,0,20)
    y_position = Height/2
    y_target = Height/2
    y_velocity = 0
    y_force = 0

    while True:
        dt = ((pygame.time.get_ticks() - last_time)/1000)+0.0001
        last_time = pygame.time.get_ticks()
        pygame.event.pump()
        screen.fill((0,0,0))

        x_velocity = x_velocity + (x_force * dt)
        x_position = x_position + (x_velocity * dt)

        y_velocity = y_velocity + (y_force * dt)
        y_position = y_position + (y_velocity * dt)

        pygame.draw.circle(screen, (255,128,255), (int((x_position)),int((y_position))), 3)
        pygame.draw.line(screen, (255,0,0), (int((x_position)),int((y_position))),  (int((x_position + (x_force/10))),int((y_position))) )
        pygame.draw.line(screen, (0,255,0), (int((x_position)),int((y_position))),  (int((x_position)),int((y_position + (y_force/10)))) )

        pygame.draw.circle(screen, (255,255,128), (int((x_target)),int((y_target))), 3)

        x_error = x_target - x_position
        y_error = y_target - y_position

        if (pygame.mouse.get_pressed()[0]):
            x_controller.reset(x_error)
            y_controller.reset(y_error)
            x_target = pygame.mouse.get_pos()[0]
            y_target = pygame.mouse.get_pos()[1]

        x_force = x_controller.process(x_error, dt)
        y_force = y_controller.process(y_error, dt)

        pygame.display.update()
